﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using Sfs2X.Entities.Data;

public class HistoryMannager : MonoBehaviour {

    private SFS sfs;

    private static HistoryMannager instance;
    public static HistoryMannager Instance { get { return instance; } }
    [SerializeField]
    GameObject Panel;
    [SerializeField]
    GameObject HistoryList;
    [SerializeField]
    GameObject HistoryItemObj;
    [SerializeField]
    Transform HistoryItemParent;
    [SerializeField]
    GameObject HistoryItemBetObj;
    [SerializeField]
    Text TxtNotification;
    [SerializeField]
    GameObject PanelDatCuoc;
    [SerializeField]
    ResultLotteryManager Result;
    [SerializeField]
    PopupLotteryManager LotteryManager;
    [SerializeField]
    GameObject Header;
    [SerializeField]
    GameObject HeaderBet;
    [SerializeField]
    Button BtnBetHistory;
    [SerializeField]
    Button Btn10DayHistory;

    private SFSObject obj;    
    ISFSArray myArray;
    private int[] DiceResult;
    private string GameResult;
    private bool ShowLichSu;
    private bool isBetToDay;
    private bool isBetTenDay;

    private Dictionary<int, HistoryItemHome> DicTen;
    private Dictionary<int, DataBetBeforResult> DicBet;

    void Awake()
    {
        instance = this;      
    }

    public void Show()
    {
        PopupLotteryManager.Panel = "LichSu";
        SendBetHistoryRequest();
        SendTenDayHistoryRequest();
        LotteryManager.BtnBet.gameObject.SetActive(true);
        PanelDatCuoc.SetActive(false);
        HistoryList.SetActive(false);
        Panel.SetActive(true);
        TxtNotification.gameObject.SetActive(false);

        BtnBetHistory.onClick.RemoveAllListeners();
        BtnBetHistory.onClick.AddListener(OnBtnBetHistoryClick);
        BtnBetHistory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        
        Btn10DayHistory.onClick.RemoveAllListeners();
        Btn10DayHistory.onClick.AddListener(OnBtn10DayHistoryClick);
        Btn10DayHistory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");

        HistoryList.SetActive(true);
        HistoryList.transform.GetChild(0).gameObject.SetActive(true);
        HistoryList.transform.GetChild(1).gameObject.SetActive(true);
        HistoryList.transform.GetChild(2).gameObject.SetActive(true);
        HistoryList.transform.GetChild(3).gameObject.SetActive(false);

        ShowLichSu = false;          
    }

    #region REQUEST

    public void SendTenDayHistoryRequest()
    {
        GamePacket gp = new GamePacket("1500");
        SFS.Instance.SendRoomRequest(gp);
        Debug.Log("da gui LotteryHistory");
    }

    public void SendBetHistoryRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.ResultBet);
        SFS.Instance.SendRoomRequest(gp);
    }

    #endregion

    #region REPONSE

    #region BET

    public void SetDataBet(GamePacket gp)
    {
        int resultIndex = gp.GetInt("ResultIndex");
        Debug.Log("Vo ham nhan cua History " + resultIndex);
        if (resultIndex == 0)
        {
            //CHƯA ĐẶT CƯỢC
            isBetToDay = false;
        }
        else if (resultIndex == 1 || resultIndex == 2)
        {
            // ĐÃ ĐẶT NHƯNG CHƯA CÓ KẾT QUẢ XỔ SỐ
            if (DicBet == null)
            {
                DicBet = new Dictionary<int, DataBetBeforResult>();
            }
            else
            {
                DicBet.Clear();
            }

            ISFSArray myArray = gp.GetSFSArray(ParamKey.ResultBet);

            Debug.Log(myArray.ToJson());
            for (int i = 0; i < myArray.Size(); i++)
            {
                SFSObject obj = (SFSObject)myArray.GetSFSObject(i);
                int indexType = obj.GetInt("Type");
                string LoaiDe = Result.GetStringTypeByIndex(indexType);
                string SoDanh = obj.GetUtfString("BetNumber");
                long TienDat = obj.GetLong("BetMoney");
                DataBetBeforResult DataBefor = new DataBetBeforResult();
                DataBefor.index = i;
                DataBefor.SoDanh = SoDanh;
                DataBefor.LoaiDe = LoaiDe;
                DataBefor.TienCuoc = TienDat.ToString();

                DicBet.Add(i, DataBefor);
            }
            isBetToDay = true;
        }
        ShowHistory();
    }

    private void ShowHistory()
    {
        for (int i = 0; i < HistoryItemParent.childCount; i++)
        {
            GameObject.Destroy(HistoryItemParent.GetChild(i).gameObject);
        }
        CheckShowItemTenDay(false);
    }
    #endregion

    #region TEN DAY

    public void SetDataTenDay(GamePacket gp)
    {
        Debug.Log("vao nhân ket qua 10 ngay LotteryHistory");
        ISFSArray myArray = gp.GetSFSArray("LotteryHistory");
        if (myArray == null)
        {
            isBetTenDay = false;
            Debug.Log("vao SetDataTenDay = null");
        }
        else
        {
            Debug.Log("vao SetDataTenDay = co");
            if (DicTen == null)
            {
                DicTen = new Dictionary<int, HistoryItemHome>();
            }
            else
            {
                DicTen.Clear();
            }

            for (int i = 0; i < myArray.Size(); i++)
            {
                SFSObject obj = (SFSObject)myArray.GetSFSObject(i);
                string day = obj.GetUtfString("Date");
                int type = obj.GetInt("Type");
                string number = obj.GetUtfString("BetNumber");
                long bet_money = obj.GetLong("BetMoney");
                long win_money = obj.GetLong("BetMoneyResult");

                string strType = "";
                if (type == 0)
                {
                    strType = "Lô 2 Số";
                }
                else if (type == 1)
                {
                    strType = "Lô 3 Số";
                }
                else if (type == 2)
                {
                    strType = "3 Càng";
                }
                else if (type == 3)
                {
                    strType = "Đề Đầu";
                }
                else if (type == 4)
                {
                    strType = "Đề Đặc Biệt";
                }
                else if (type == 5)
                {
                    strType = "Đánh Đầu";
                }
                else if (type == 6)
                {
                    strType = "Đánh Đuôi";
                }

                HistoryItemHome item = new HistoryItemHome();
                item.Date = day;
                item.Type = strType;
                item.Number = number;
                item.Money = bet_money.ToString();
                item.MoneyWin = win_money.ToString();

                DicTen.Add(i, item);

            }
            isBetTenDay = true;
        }
        Debug.Log("isBetTenDay = " + isBetTenDay);
    }

    #endregion

    #endregion

    #region SHOW ITEM

    public void CheckShowItemTenDay(bool show = true)
    {
        for (int i = 0; i < HistoryItemParent.childCount; i++)
        {
            GameObject.Destroy(HistoryItemParent.GetChild(i).gameObject);
        }

        if (show == true)
        {
            Header.SetActive(true);
            HeaderBet.SetActive(false);
            ShowItem(true);
        }
        else
        {
            Header.SetActive(false);
            HeaderBet.SetActive(true);
            ShowItem(false);
        }
    }


    private void ShowItem(bool showItem10Day = true)
    {
        if (showItem10Day == true)
        {
            if (isBetTenDay == true)
            {
                foreach (KeyValuePair<int, HistoryItemHome> item in DicTen)
                {
                    string date = item.Value.Date;
                    string type = item.Value.Type;
                    string number = item.Value.Number;
                    string bet_money = item.Value.Money;
                    string win_money = item.Value.MoneyWin;

                    ShowItemTenDay(item.Key, date, type, number, bet_money, win_money);

                }
            }
            else
            {
                TxtNotification.gameObject.SetActive(true);
                TxtNotification.text = "10 NGÀY GẦN NHẤT BẠN CHƯA BẶT CƯỢC";
            }
        }
        else
        {
            if (isBetToDay == true)
            {
                foreach (KeyValuePair<int, DataBetBeforResult> item in DicBet)
                {
                    string type = item.Value.LoaiDe;
                    string number = item.Value.SoDanh;
                    string money = item.Value.TienCuoc;
                    ShowItemBet(item.Key, type, number, money);
                }
            }
            else
            {
                TxtNotification.gameObject.SetActive(true);
                TxtNotification.text = "HÔM NAY BẠN CHƯA BẶT CƯỢC";
            }
        }
    }

    private void ShowItemTenDay(int index, string date, string type, string number, string money, string moneywin)
    {
        HistoryModule itemView;
        GameObject obj = Instantiate(HistoryItemObj) as GameObject;
        HistoryItemHome item = new HistoryItemHome();
        obj.transform.SetParent(HistoryItemParent);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<HistoryModule>();
        itemView.Init(item);
        itemView.Show(date, type, number, money, moneywin);
    }

    private void ShowItemBet(int index, string type, string number, string money)
    {
        HistotyBetModule itemView;
        GameObject obj = Instantiate(HistoryItemBetObj) as GameObject;
        HistoryBetItemHome item = new HistoryBetItemHome();
        obj.transform.SetParent(HistoryItemParent);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<HistotyBetModule>();
        itemView.Init(item);
        itemView.Show(type, number, money);
    }

    #endregion

    public void OnBtnBetHistoryClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        TxtNotification.gameObject.SetActive(false);
        BtnBetHistory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        Btn10DayHistory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        CheckShowItemTenDay(false);

    }

    public void OnBtn10DayHistoryClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        TxtNotification.gameObject.SetActive(false);
        BtnBetHistory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        Btn10DayHistory.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        CheckShowItemTenDay(true);

    }


    public void OffTxtNotification()
    {
        TxtNotification.text = "";
        TxtNotification.gameObject.SetActive(false);
    }

    public void Hide()
    {
        Panel.SetActive(false);
    }


    public void UpdateHistory(int[] diceResult, string gameResult)
    {
        DiceResult = diceResult;
        GameResult = gameResult;

        if (Panel.activeSelf)
            Show();
    }
}

public class HistoryItemHome
{
    public string Date { get; set; }
    public string Type { get; set; }
    public string Number { get; set; }
    public string Money { get; set; }
    public string MoneyWin { get; set; }
}

public class HistoryBetItemHome
{
    public string Type { get; set; }
    public string Number { get; set; }
    public string Money { get; set; }
}


