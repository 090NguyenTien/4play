﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class ItemPlayerInviteView : MonoBehaviour {

	[SerializeField]
		Button btn;
	[SerializeField]
		Text txtName, txtChip;
	[SerializeField]
		Image imgAvatar, imgBorderAvt;

		onCallBackInt _itemOnClick;

		int id;

	public void Init(bool _odd, int _id, string _avtName, string _name, long _chip, int _vipPoint, onCallBackInt _onClick)
		{
		
		btn.onClick.RemoveAllListeners ();
		btn.onClick.AddListener (ItemOnClick);

				id = _id;
				txtName.text = _name;
		txtChip.text = Utilities.GetStringMoneyByLong (_chip);

		imgBorderAvt.gameObject.SetActive (false);
        //		imgBorderAvt.sprite = DataHelper.GetVip (_vipPoint);
        if (_avtName.Contains("http") == true)
        {
            StartCoroutine(DataHelper.UpdateAvatarThread(_avtName, imgAvatar));
        }
        else
        {
            //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
            StartCoroutine(DataHelper.UpdateAvatarThread(API.PREFIX_AVT + _avtName, imgAvatar));
        }
        //GetSpriteSuccess (DataHelper.GetAvatar (_avtName));
        //StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + userInfo.Avatar, imgAvatar));
        _itemOnClick = null;
				_itemOnClick = _onClick;
	}
		void ItemOnClick()
		{
				_itemOnClick (id);
		}

		void GetSpriteSuccess(Sprite _spr)
		{
				imgAvatar.sprite = _spr;
		}
}
