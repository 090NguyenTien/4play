﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using BaseCallBack;


public class ItemBoomControll : MonoBehaviour
{

    RectTransform MyTransform;
    Vector3 TargetPos;
    [SerializeField]
    GameObject ObjBoomNo;
    [SerializeField]
    Transform trsfLuckyWheel;

    [SerializeField]
    Transform targetMove;
    onCallBack SendRequestBoom;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Init(Transform _targetMove)
    {
        MyTransform = this.gameObject.GetComponent<RectTransform>();
        targetMove = _targetMove;
        TargetPos = targetMove.transform.position;
    }

    public void NemBoom(onCallBack _SendRequestBoom, int LoaiBom = 0)
    {
        MyTransform = this.gameObject.GetComponent<RectTransform>();
        SoundManager.PlaySound(SoundManager.NEM_BOM);
        // Debug.Log("targetMove " + targetMove);
        TargetPos = targetMove.transform.position;
        SendRequestBoom = _SendRequestBoom;
        // trsfLuckyWheel.DORotate(new Vector3(0, 0, -720), 5, RotateMode.FastBeyond360);
        Tweener myTweener = trsfLuckyWheel.DORotate(new Vector3(0, 0, -720), 5, RotateMode.FastBeyond360);
        MyTransform.DOMove(TargetPos, 1f).OnComplete(() =>
        {
            // trsfLuckyWheel.DORotate(new Vector3(0, 0, -900), 5, RotateMode.FastBeyond360).Pause();
            myTweener.Pause();
            MyTransform.rotation = Quaternion.Euler(0, 0, 0);

            Destroy(this.gameObject, 0.75f);
            if (LoaiBom == 0)
            {
                SoundManager.PlaySound(SoundManager.DAP_HEO);
            }
            else if (LoaiBom >= 1)
            {
                SoundManager.PlaySound(SoundManager.DANH_1_LA);
            }

            ObjBoomNo.SetActive(true);
            SendRequestBoom();
        });


    }

}
