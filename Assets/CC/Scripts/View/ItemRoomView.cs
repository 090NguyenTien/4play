﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using BaseCallBack;

public class ItemRoomView : MonoBehaviour {

    private Image imgBg;
    private Button btnItemRoom;
    private Text txtRoomId, txtHost, txtBet;
    private GameObject objLock;
    //private Transform trsfParentPlayer;

    onCallBackIntBool onClick;
    bool isPlaying;
    int roomID;
    bool isPass;
    [SerializeField]
    private Text txtPlayerQuantity = null;
    [SerializeField]
    private Image imgPlayer = null;

    [SerializeField]
    private Text txtChipRequire = null;

    public ItemRoomInfo itemRoomInfo = null;

    public void Init()
    {
        imgBg = GetComponent<Image>();

        btnItemRoom = GetComponent<Button>();
        btnItemRoom.onClick.AddListener(ItemRoomOnClick);

        txtRoomId = transform.GetChild(0).GetComponent<Text>();
        txtHost = transform.GetChild(1).GetComponent<Text>();
        txtBet = transform.GetChild(2).GetComponent<Text>();

        objLock = transform.GetChild(4).gameObject;
        //trsfParentPlayer = transform.GetChild(3).transform;

        isPass = false;
    }
    public void SetSoloPlayerMax(ItemRoomInfo _room)
    {
        txtPlayerQuantity.text = _room.PlayerCurrent.ToString() + "/2";
        imgPlayer.fillAmount = _room.PlayerCurrent / 2f;
    }
    public void Show(ItemRoomInfo _room, bool _indexOld, bool _isPass, onCallBackIntBool _callBack)
	{
        itemRoomInfo = _room;

        txtRoomId.text = _room.ID.ToString ();

        txtChipRequire.text = Utilities.GetStringMoneyByLong(_room.ChipRequire);

        txtBet.text = Utilities.GetStringMoneyByLong(_room.Bet);

		txtHost.text = _room.Host;

		isPass = _isPass;

        txtPlayerQuantity.text = _room.PlayerCurrent.ToString() + "/" + _room.PlayerMax.ToString();
        isPlaying = _room.IsPlaying;
        if (isPlaying)
        {
            txtPlayerQuantity.supportRichText = true;
            txtPlayerQuantity.text = "<color=yellow>" + "(Đang Chơi)" + " </color>";
        }
        
        imgPlayer.fillAmount = _room.PlayerCurrent / (float)_room.PlayerMax;
//        for (int i = 0; i < _room.PlayerMax; i++)
//        {
//			trsfParentPlayer.GetChild (i).gameObject.SetActive (i < _room.PlayerMax);
//			trsfParentPlayer.GetChild (i).GetChild (0).gameObject.SetActive (i < _room.PlayerCurrent);

////			imgBg.enabled = _indexOld;
//			imgBg.color = _indexOld ? new Color(0, 0, 0, .3f) : new Color (0, 0, 0, .1f);
//		}
		objLock.SetActive (_isPass);

		roomID = _room.ID;

		onClick = _callBack;
	}

		void ItemRoomOnClick()
		{
		        SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
            
				onClick (roomID, isPlaying);
		}
}
