﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ItemTopEvenTaiXiu : MonoBehaviour
{
    [SerializeField]
    Text TxtName, TxtTien;

    public void Init(string ten, string tien)
    {
        TxtName.text = ten;
        TxtTien.text = tien;
    }
}
