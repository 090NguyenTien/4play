﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaneEvenTaiXiu : MonoBehaviour
{
    [SerializeField]
    ShopEvenTaiXiu ShopEven;
    [SerializeField]
    Button BtnNap, BtnChoi;
    [SerializeField]
    GameObject Me, ObjShop, ContentTopChip, ItemTopEven;
    [SerializeField]
    Image Avatar;
    [SerializeField]
    Text TxtTen, TxtTienThang, TxtTienNap;
    public float ThoiGianCapNhatTop = 5f;


    public void Init()
    {
        BtnNap.onClick.RemoveAllListeners();
        BtnNap.onClick.AddListener(OpenShop);

        InitTop("Tony", "678M", "0");
        /*
        for (int i = 0; i < 9; i++)
        {
            TaoItemTop("Tony", "689M");
        }
        */
        
        Me.SetActive(true);
        CapNhatTop();
    }


    public void InitTop(string ten, string tien, string tienthang)
    {
        Avatar.sprite = MyInfo.sprAvatar;
        TxtTen.text = ten;
        TxtTienNap.text = tien;
        TxtTienThang.text = tienthang;
    }


    public void OpenShop()
    {
        ObjShop.SetActive(true);
    }


    public void TaoItemTop(string ten, string chip)
    {
        GameObject it = Instantiate(ItemTopEven, ContentTopChip.transform) as GameObject;

        ItemTopEvenTaiXiu itcs = it.GetComponent<ItemTopEvenTaiXiu>();
        itcs.Init(ten, chip);
    }


    public void XoaContentTop()
    {
        int cn = ContentTopChip.transform.childCount;
        for (int i = 0; i < cn; i++)
        {
            GameObject it = ContentTopChip.transform.GetChild(i).gameObject as GameObject;
            Destroy(it);
        }
    }

    public void CapNhatTop()
    {
        XoaContentTop();
        Debug.LogWarning("Cap Nhat Top Ne ");
        //Nhan danh sach moi

        for (int i = 0; i < 9; i++)
        {
            TaoItemTop("Tony", "689M");
        }
        StartCoroutine(ChoCapNhat());
    }

    public IEnumerator ChoCapNhat()
    {
        yield return new WaitForSeconds(ThoiGianCapNhatTop);
        CapNhatTop();
    }

    public void BtnRefreshTopOnClick()
    {
        StopCoroutine(ChoCapNhat());
        Debug.LogWarning("DungNe");
        CapNhatTop();
    }



}
