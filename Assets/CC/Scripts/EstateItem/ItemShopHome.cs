﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShopHome : MonoBehaviour
{
    [SerializeField]
    Image ImgItem, ImgUse;
    [SerializeField]
    GameObject ImgClock, _DiamonPrice, _ChipPrice;
    [SerializeField]
    Text TxtNameItem, TxtDiamonPrice, TxtChipPrice, TxtPoint, TxtCount;

    [SerializeField]
    MadControll Mad;
    [SerializeField]
    TestItemInHome PanelBuy;




    public long LongDiamonPrice, LongChipPrice, Save_Chip, Save_Gem;

    public string Ten, ThongTin, Id, Img;

    public int DiemTaiSan, stt, soluong;

    public bool IsClock = true;
    public string MyId = "";
    Button MyButton;



    public void LoadHinh()
    {
        // API.Instance.Load_SprHouse()
    }

    public void Init(string Id, bool Clock, string Count, bool isSale = false, long PercentSale = 0)
    {
        MyId = Id;
        IsClock = Clock;


        My_Item it = DataHelper.DuLieuNha[Id];

        Img = it.img;
        API.Instance.Load_SprItem(Img, ImgItem);

        // ImgItem.sprite = DataHelper.dictSprite_House[Id];
        TxtNameItem.text = it.ten;
        MyButton = this.gameObject.GetComponent<Button>();
        LongDiamonPrice = it.gem;
        LongChipPrice = it.chip;
        TxtPoint.text = it.diemtaisan.ToString();
        DiemTaiSan = it.diemtaisan;
        ThongTin = it.thongtin;
        XuLyChuoiThongTin();

        TxtCount.text = Count;
        TxtCount.gameObject.SetActive(true);
        if (IsClock == true)
        {
            GameObject obj = GameObject.Find("PanelUI_Home");
            EstateControll estate = obj.GetComponent<EstateControll>();
            GameObject panell = estate.PanelBuyItem;
            PanelBuy = panell.GetComponent<TestItemInHome>();

            // PanelBuy = GameObject.Find("PanelBuyItem").GetComponent<TestItemInHome>();

            TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(LongDiamonPrice);
            if (LongChipPrice == 0)
            {
                _ChipPrice.SetActive(false);
            }
            else
            {
                TxtChipPrice.text = Utilities.GetStringMoneyByLong(LongChipPrice);
                _ChipPrice.SetActive(true);
            }

            _DiamonPrice.SetActive(true);

            ImgClock.SetActive(true);
            MyButton.onClick.RemoveAllListeners();
            MyButton.onClick.AddListener(BtnClockClick);
        }
        else
        {
            Mad = GameObject.Find("Mad").GetComponent<MadControll>();
            _DiamonPrice.SetActive(false);
            _ChipPrice.SetActive(false);
            ImgClock.SetActive(false);
            ImgUse.gameObject.SetActive(true);
            MyButton.onClick.RemoveAllListeners();
            MyButton.onClick.AddListener(BtnClick);
        }
    }


    public void XuLyChuoiThongTin()
    {
        ThongTin = ThongTin.Remove(0, 3);

        int i = ThongTin.IndexOf('<');
        ThongTin = ThongTin.Remove(i, 7);
        ThongTin = ThongTin.Insert(i, "\n");

        int u = ThongTin.IndexOf('<');
        int c = ThongTin.Length - 1;

        if (c - u > 3)
        {
            ThongTin = ThongTin.Remove(u, 7);
            ThongTin = ThongTin.Insert(u, "\n");

            int t = ThongTin.IndexOf('<');
            int s = ThongTin.Length;
            ThongTin = ThongTin.Remove(t, s - t);
        }
        else
        {
            ThongTin = ThongTin.Remove(u, 4);
        }


    }




    public void BtnClick()
    {
        Mad.ChangeHome(MyId);
    }


    public void BtnClockClick()
    {
        // Mad.ChangeHome(MyId);
        PanelBuy.Init(MyId, TxtNameItem.text, ThongTin, LongDiamonPrice, LongChipPrice, DiemTaiSan);
        PanelBuy.Show();
    }

}
