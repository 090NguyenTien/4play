﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class MadControll : MonoBehaviour
{
    [SerializeField]
    Image ImgHome;
    [SerializeField]
    EstateControll estateControll;



    [SerializeField]
    public List<Sprite> SpritesHome;
    [SerializeField]
    public List<Sprite> SpriteItemHome;
    [SerializeField]
    public List<Sprite> SpriteShops;
    [SerializeField]
    public List<Sprite> SpriteCar;
    [SerializeField]
    public List<Sprite> SpriteItemCar;
    [SerializeField]
    GameObject imgAdd_Shop_1, imgAdd_Shop_2, imgAdd_Shop_3, imgAdd_Shop_4, imgAdd_Shop_5;

    [SerializeField]
    GameObject imgAdd_Car_1, imgAdd_Car_2;

    public string ID_House;

    [SerializeField]
    UserInfo userInfo;



    string StringHomeUnClock = "";
    string StringShopUnClock = "";
    string StringCarUnClock = "";

    string StringView = "3,1,2,3,4,5,6,7";






    public string iD_Shop_1, iD_Shop_2, iD_Shop_3, iD_Shop_4, iD_Shop_5;

    public string iD_Car_1, iD_Car_2;

    public Image Shop_1, Shop_2, Shop_3, Shop_4, Shop_5;

    public Image Car_1, Car_2;

    public int ID_CarToChange;

    public int ID_ShopToChange;

    public Sprite SprKhung;
    public Sprite SprNhaMacDinh;

    void Start()
    {
        //  Init();
        //XemNha(StringView);
    }



    public void XemNha()
    {

        if (DataHelper.DuLieuMap_Enemy[0] == "")
        {
            ID_House = "5dc03420e52b8e2d238b6045";
        }
        else
        {
            if (DataHelper.DuLieuMap_Enemy[0] == null)
            {
                ID_House = "5dc03420e52b8e2d238b6045";
            }
            else
            {
                ID_House = DataHelper.DuLieuMap_Enemy[0];
            }

        }






        if (DataHelper.DuLieuMap_Enemy[1] == null)
        {
            iD_Shop_1 = "";
        }
        else
        {
            iD_Shop_1 = DataHelper.DuLieuMap_Enemy[1];
        }

        if (DataHelper.DuLieuMap_Enemy[2] == null)
        {
            iD_Shop_2 = "";
        }
        else
        {
            iD_Shop_2 = DataHelper.DuLieuMap_Enemy[2];
        }

        if (DataHelper.DuLieuMap_Enemy[3] == null)
        {
            iD_Shop_3 = "";
        }
        else
        {
            iD_Shop_3 = DataHelper.DuLieuMap_Enemy[3];
        }

        if (DataHelper.DuLieuMap_Enemy[4] == null)
        {
            iD_Shop_4 = "";
        }
        else
        {
            iD_Shop_4 = DataHelper.DuLieuMap_Enemy[4];
        }

        if (DataHelper.DuLieuMap_Enemy[5] == null)
        {
            iD_Shop_5 = "";
        }
        else
        {
            iD_Shop_5 = DataHelper.DuLieuMap_Enemy[5];
        }


        if (DataHelper.DuLieuMap_Enemy[6] == null)
        {
            iD_Car_1 = "";
        }
        else
        {
            iD_Car_1 = DataHelper.DuLieuMap_Enemy[6];
        }

        if (DataHelper.DuLieuMap_Enemy[7] == null)
        {
            iD_Car_2 = "";
        }
        else
        {
            iD_Car_2 = DataHelper.DuLieuMap_Enemy[7];
        }


        string TenImgNha = DataHelper.DuLieuTenImgTheoID[ID_House];
        API.Instance.Load_SprItem(TenImgNha, ImgHome);
        // Debug.LogWarning("LAY HINH NHA NE  " + hinhNha);
        /*
                if (hinhNha == null)
                {
                    ImgHome.sprite = SprNhaMacDinh;
                }
                else
                {
                    ImgHome.sprite = hinhNha;
                }
        */


        HienThiHinh(Shop_1, iD_Shop_1);
        HienThiHinh(Shop_2, iD_Shop_2);
        HienThiHinh(Shop_3, iD_Shop_3);
        HienThiHinh(Shop_4, iD_Shop_4);
        HienThiHinh(Shop_5, iD_Shop_5);

        HienThiHinh(Car_1, iD_Car_1, true);
        HienThiHinh(Car_2, iD_Car_2, true);
    }


    public void InitSpr()
    {
        Dictionary<string, Sprite> t = DataHelper.dictSprite_House;
        SpritesHome.Clear();
        SpriteItemHome.Clear();
        foreach (var item in t)
        {
            SpritesHome.Add(item.Value);
            SpriteItemHome.Add(item.Value);
        }

        Dictionary<string, Sprite> sh = DataHelper.dictSprite_Shop;
        SpriteShops.Clear();
        SpriteShops.Add(SprKhung);
        foreach (var item in sh)
        {
            SpriteShops.Add(item.Value);
        }



        Dictionary<string, Sprite> ca = DataHelper.dictSprite_Car;
        SpriteCar.Clear();
        SpriteCar.Add(SprKhung);
        SpriteItemCar.Clear();
        SpriteItemCar.Add(SprKhung);
        foreach (var item in ca)
        {
            SpriteCar.Add(item.Value);
            SpriteItemCar.Add(item.Value);
        }


        DataHelper.IsInitShopVatPham = true;
        //  Debug.LogWarning("Da tao Hinh xong");
    }




    public void Init()
    {
        // Lấy Data


        if (DataHelper.DuLieuMap[0] == "")
        {
            ID_House = "5dc03420e52b8e2d238b6045";
        }
        else
        {
            if (DataHelper.DuLieuMap[0] == null)
            {
                ID_House = "5dc03420e52b8e2d238b6045";
            }
            else
            {
                ID_House = DataHelper.DuLieuMap[0];
            }

        }


        if (DataHelper.DuLieuMap[1] == null)
        {
            iD_Shop_1 = "";
        }
        else
        {
            iD_Shop_1 = DataHelper.DuLieuMap[1];
        }

        if (DataHelper.DuLieuMap[2] == null)
        {
            iD_Shop_2 = "";
        }
        else
        {
            iD_Shop_2 = DataHelper.DuLieuMap[2];
        }

        if (DataHelper.DuLieuMap[3] == null)
        {
            iD_Shop_3 = "";
        }
        else
        {
            iD_Shop_3 = DataHelper.DuLieuMap[3];
        }

        if (DataHelper.DuLieuMap[4] == null)
        {
            iD_Shop_4 = "";
        }
        else
        {
            iD_Shop_4 = DataHelper.DuLieuMap[4];
        }

        if (DataHelper.DuLieuMap[5] == null)
        {
            iD_Shop_5 = "";
        }
        else
        {
            iD_Shop_5 = DataHelper.DuLieuMap[5];
        }


        if (DataHelper.DuLieuMap[6] == null)
        {
            iD_Car_1 = "";
        }
        else
        {
            iD_Car_1 = DataHelper.DuLieuMap[6];
        }

        if (DataHelper.DuLieuMap[7] == null)
        {
            iD_Car_2 = "";
        }
        else
        {
            iD_Car_2 = DataHelper.DuLieuMap[7];
        }




        //    StringHomeUnClock = _StringHomeUnClock;
        //    StringShopUnClock = _StringShopUnClock;
        //    StringCarUnClock = _StringCarUnClock;

        // Xữ lý Data

        // Hiển thị 


        string TenImgNha = DataHelper.DuLieuTenImgTheoID[ID_House];
        API.Instance.Load_SprItem(TenImgNha, ImgHome);
        /*
                Debug.LogWarning("LAY HINH NHA NE  " + hinhNha);

                if (hinhNha == null)
                {
                    ImgHome.sprite = SprNhaMacDinh;
                }
                else
                {
                    ImgHome.sprite = hinhNha;
                }
        */
        /*

                if (DataHelper.dictSprite_House[ID_House] == null)
                {
                    ImgHome.sprite = SprNhaMacDinh;
                }
                else
                {
                    ImgHome.sprite = DataHelper.dictSprite_House[ID_House];
                }

        */


        HienThiHinh(Shop_1, iD_Shop_1);
        HienThiHinh(Shop_2, iD_Shop_2);
        HienThiHinh(Shop_3, iD_Shop_3);
        HienThiHinh(Shop_4, iD_Shop_4);
        HienThiHinh(Shop_5, iD_Shop_5);

        HienThiHinh(Car_1, iD_Car_1, true);
        HienThiHinh(Car_2, iD_Car_2, true);




        CheckShop();
        CheckCar();

    }



    void HienThiHinh(Image img, string id, bool iscar = false)
    {
        if (id != "")
        {
            string tenImg = DataHelper.DuLieuTenImgTheoID[id];

            API.Instance.Load_SprItem(tenImg, img);
            /*
            Sprite hinh = API.Instance.Load_SprItem(tenImg);

            Debug.LogWarning("LAY HINH SHOP NE  " + hinh);
            if (iscar == false)
            {
                if (hinh == null)
                {
                    img.sprite = SprKhung;
                }
                else
                {
                    img.sprite = hinh;
                }
                
            }
            else
            {
                if (hinh == null)
                {
                    img.sprite = SprKhung;
                }
                else
                {
                    img.sprite = hinh;
                }
                
            }
            */


        }
        else
        {
            img.sprite = SprKhung;
        }
    }




    public void XuLySoLuongItemConLaiTrongKho()
    {/*
        for (int i = 1; i < 6; i++)
        {
            string id_shop_inMad = DataItemInMad[i];
            if (id_shop_inMad != "0")
            {
                string temp = DataItemShopUnClock[int.Parse(id_shop_inMad)];
                int t = int.Parse(temp);
                t--;
                DataItemShopUnClock[int.Parse(id_shop_inMad)] = t.ToString();
            }
            
        }
        for (int i = 6; i < 8; i++)
        {
            string id_car_inMad = DataItemInMad[i];
            if (id_car_inMad != "0")
            {
                string temp = DataItemCarUnClock[int.Parse(id_car_inMad)];
                int t = int.Parse(temp);
                t--;
                DataItemCarUnClock[int.Parse(id_car_inMad)] = t.ToString();
            }          
        }
        */
    }


    public void CheckShop()
    {
        if (iD_Shop_1 == "")
        {
            imgAdd_Shop_1.SetActive(true);
        }
        else
        {
            imgAdd_Shop_1.SetActive(false);
        }

        if (iD_Shop_2 == "")
        {
            imgAdd_Shop_2.SetActive(true);
        }
        else
        {
            imgAdd_Shop_2.SetActive(false);
        }

        if (iD_Shop_3 == "")
        {
            imgAdd_Shop_3.SetActive(true);
        }
        else
        {
            imgAdd_Shop_3.SetActive(false);
        }

        if (iD_Shop_4 == "")
        {
            imgAdd_Shop_4.SetActive(true);
        }
        else
        {
            imgAdd_Shop_4.SetActive(false);
        }

        if (iD_Shop_5 == "")
        {
            imgAdd_Shop_5.SetActive(true);
        }
        else
        {
            imgAdd_Shop_5.SetActive(false);
        }
    }


    public void CheckCar()
    {
        if (iD_Car_1 == "")
        {
            imgAdd_Car_1.SetActive(true);
        }
        else
        {
            imgAdd_Car_1.SetActive(false);
        }

        if (iD_Car_2 == "")
        {
            imgAdd_Car_2.SetActive(true);
        }
        else
        {
            imgAdd_Car_2.SetActive(false);
        }


    }





    public void ChangeHome(string Id)
    {
        estateControll.CloseItemScroll();
        // ImgHome.sprite = DataHelper.dictSprite_House[Id];

        string Img = DataHelper.DuLieuTenImgTheoID[Id];
        API.Instance.Load_SprItem(Img, ImgHome);

        int count = DataHelper.KhoItem[Id];


        string curentId = ID_House;


        if (count != 0 && curentId != Id)
        {
            count--;
            DataHelper.KhoItem[Id] = count;

            if (DataHelper.KhoItem.ContainsKey(curentId))
            {
                DataHelper.KhoItem[curentId] = DataHelper.KhoItem[curentId] + 1;
            }
        }



        ID_House = Id;
        Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id);
        API.Instance.RequestDatVatPhamTrenMad(Id, 0, RspDatVatPhamTrenMad);


        estateControll.OpenBtnController();
    }




    void RspDatVatPhamTrenMad(string _json)
    {
        Debug.LogWarning("DU LIEU Dat Vat Pham: " + _json);
        JSONNode node = JSONNode.Parse(_json);


        string sta = node["status"].Value;
        Debug.LogWarning("DU LIEU Dat Vat Pham  STA: " + sta);
        if (sta == "1")
        {
            JSONNode data = node["placed_assets"];
            Debug.LogWarning("DU LIEU Dat Vat Pham  placed_assets: " + data);


            int cou = data.Count;

            for (int i = 0; i < cou; i++)
            {
                int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                string a_id = node["placed_assets"][i]["id"].Value;

                // Debug.LogError("id ---------- " + a_id + "pos ---------- " + pos);
                DataHelper.DuLieuMap[pos] = a_id;
            }

        }
        else
        {
            string smg = node["msg"].Value;
            Debug.LogWarning("DU LIEU Dat Vat Pham loiiiiiiiii: " + smg);
        }
    }






    public void ChangeShop(string Id)
    {
        estateControll.CloseItemScroll();
        string curentId = "";
        if (ID_ShopToChange == 1)
        {
            curentId = iD_Shop_1;
            iD_Shop_1 = Id;
            CheckShop();
            //Shop_1.sprite = DataHelper.dictSprite_Shop[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, Shop_1);

            //  Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 1" );
            API.Instance.RequestDatVatPhamTrenMad(Id, 1, RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 2)
        {
            curentId = iD_Shop_2;
            iD_Shop_2 = Id;
            CheckShop();
            //Shop_2.sprite = DataHelper.dictSprite_Shop[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, Shop_2);
            //   Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 2");
            API.Instance.RequestDatVatPhamTrenMad(Id, 2, RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 3)
        {
            curentId = iD_Shop_3;
            iD_Shop_3 = Id;
            CheckShop();
            // Shop_3.sprite = DataHelper.dictSprite_Shop[Id];
            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, Shop_3);


            //   Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 3");
            API.Instance.RequestDatVatPhamTrenMad(Id, 3, RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 4)
        {
            curentId = iD_Shop_4;
            iD_Shop_4 = Id;
            CheckShop();
            //  Shop_4.sprite = DataHelper.dictSprite_Shop[Id];
            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, Shop_4);

            //    Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 4");
            API.Instance.RequestDatVatPhamTrenMad(Id, 4, RspDatVatPhamTrenMad);
        }
        else if (ID_ShopToChange == 5)
        {
            curentId = iD_Shop_5;
            iD_Shop_5 = Id;
            CheckShop();
            //  Shop_5.sprite = DataHelper.dictSprite_Shop[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, Shop_5);
            //  Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 5");
            API.Instance.RequestDatVatPhamTrenMad(Id, 5, RspDatVatPhamTrenMad);
        }

        int count = DataHelper.KhoItem[Id];
        if (count != 0 && curentId != Id)
        {
            count--;
            DataHelper.KhoItem[Id] = count;

            if (DataHelper.KhoItem.ContainsKey(curentId))
            {
                DataHelper.KhoItem[curentId] = DataHelper.KhoItem[curentId] + 1;
            }
        }

        estateControll.OpenBtnController();
    }

    public void DontChange()
    {
        estateControll.CloseItemScroll();
        estateControll.OpenBtnController();
    }



    public void ChangeCar(string Id)
    {
        estateControll.CloseItemScroll();

        string curentId = "";

        if (ID_CarToChange == 1)
        {
            curentId = iD_Car_1;
            iD_Car_1 = Id;
            CheckCar();
            //Car_1.sprite = DataHelper.dictSprite_Car[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, Car_1);

            //   Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 6");
            API.Instance.RequestDatVatPhamTrenMad(Id, 6, RspDatVatPhamTrenMad);
        }
        else if (ID_CarToChange == 2)
        {
            curentId = iD_Car_2;
            iD_Car_2 = Id;
            CheckCar();
            //  Car_2.sprite = DataHelper.dictSprite_Car[Id];

            string Img = DataHelper.DuLieuTenImgTheoID[Id];
            API.Instance.Load_SprItem(Img, Car_2);
            //  Debug.LogWarning("DU LIEU Dat Vat Pham ID: " + Id + "    pos = 7");
            API.Instance.RequestDatVatPhamTrenMad(Id, 7, RspDatVatPhamTrenMad);
        }


        int count = DataHelper.KhoItem[Id];
        if (count != 0 && curentId != Id)
        {
            count--;
            DataHelper.KhoItem[Id] = count;
            if (DataHelper.KhoItem.ContainsKey(curentId))
            {
                DataHelper.KhoItem[curentId] = DataHelper.KhoItem[curentId] + 1;
            }

        }

        estateControll.OpenBtnController();
    }

    public void BuyHome(string Id)
    {
        //  DataItemHomeUnClock[Id] = "1";
        // ID_House = Id;



    }

    public void BuyShop(string Id)
    {
        //  int countCurent = int.Parse(DataItemShopUnClock_Start[Id]);
        // countCurent++;
        // DataItemShopUnClock_Start[Id] = countCurent.ToString();
    }

    public void BuyCar(string Id)
    {
        // int countCurent = int.Parse(DataItemCarUnClock_Start[Id]);
        // countCurent++;
        // DataItemCarUnClock_Start[Id] = countCurent.ToString();
    }

    public string GetStringDataInMad()
    {
        string Data = ID_House.ToString() + "," + iD_Shop_1.ToString() + "," + iD_Shop_2.ToString() + "," + iD_Shop_3.ToString() + "," + iD_Shop_4.ToString() + "," + iD_Shop_5.ToString() + "," + iD_Car_1.ToString() + "," + iD_Car_2.ToString();
        return Data;
    }

    public string GetStringDataHome()
    {

        string data = "";
        /*
        for (int i = 0; i < DataItemHomeUnClock.Length; i++)
        {
            if (i < DataItemHomeUnClock.Length - 1)
            {
                data += DataItemHomeUnClock[i] + ",";
            }
            else
            {
                data += DataItemHomeUnClock[i];
            }            
        }
        */
        return data;
    }

    public string GetStringDataShop()
    {
        string data = "";
        /*
        for (int i = 0; i < DataItemShopUnClock_Start.Length; i++)
        {
            if (i < DataItemShopUnClock_Start.Length - 1)
            {
                data += DataItemShopUnClock_Start[i] + ",";
            }
            else
            {
                data += DataItemShopUnClock_Start[i];
            }
        }
        */
        return data;
    }

    public string GetStringDataCar()
    {
        string data = "";
        /*
        for (int i = 0; i < DataItemCarUnClock_Start.Length; i++)
        {
            if (i < DataItemCarUnClock_Start.Length - 1)
            {
                data += DataItemCarUnClock_Start[i] + ",";
            }
            else
            {
                data += DataItemCarUnClock_Start[i];
            }
        }
        */
        return data;
    }

    public void BackAndSave()
    {/*
        string dataInMad, dataHome, dataShop, dataCar;

        dataInMad = GetStringDataInMad();
        dataHome = GetStringDataHome();
        dataShop = GetStringDataShop();
        dataCar = GetStringDataCar();

        userInfo.DataInMad_Change = GetStringDataInMad();
        userInfo.StringHomeUnClock_Change = GetStringDataHome();
        userInfo.StringShopUnClock_Change = GetStringDataShop();
        userInfo.StringCarUnClock_Change = GetStringDataCar();

        Debug.LogWarning("dataInMad = " + dataInMad);
        Debug.LogWarning("dataHome = " + dataHome);
        Debug.LogWarning("dataShop = " + dataShop);
        Debug.LogWarning("dataCar = " + dataCar);
        */
    }


}
