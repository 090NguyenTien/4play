﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopupModuleInGameManager : MonoBehaviour {

	[SerializeField]
	GameObject panel;

	[SerializeField]
	GameObject SoundOn, SoundOff, InvitedOn, InvitedOff, BackgroundOn, BackgroundOff;
	[SerializeField]
	Button btnModule, btnSound, btnInvited, btnClose, btnBackgroundMusic;
	[SerializeField]
	Text txtPingTime;
	[SerializeField]
	Image imgPing;
	[SerializeField]
	Sprite sprOn, sprOff;

	void Awake()
	{
		SettingHelper.Instance.Init ();

		btnModule.onClick.AddListener (BtnModuleOnClick);

			btnSound.onClick.AddListener (SoundOnClick);
			ShowSound (SettingHelper.SOUND.ENABLE);



			btnInvited.onClick.AddListener (InvitedOnClick);
			ShowInvited (SettingHelper.Instance.INVITED);

        btnBackgroundMusic.onClick.AddListener(SoundBackgroundMusicClick);
        ShowBackgroundMusic(SettingHelper.BACKGROUND_MUSIC.ENABLE);


        btnClose.onClick.AddListener (CloseOnClick);
	}

	void BtnModuleOnClick ()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		Show ();
	}

	[SerializeField]
	GameObject wifi1, wifi2, wifi3;

	public void ShowPing(string _value){
//		txtPingTime.text = _value + " ms";

//		imgPing.sprite = long.Parse (_value) > 500 ? sprOff : sprOn;

		wifi3.SetActive (long.Parse (_value) < 50);
		wifi2.SetActive (long.Parse (_value) < 500);
		wifi1.SetActive (long.Parse (_value) < 1000);
	}

	public void Show()
	{
		panel.SetActive (true);
	}
	public void Hide()
	{
		panel.SetActive (false);
	}

	void CloseOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		Hide ();
	}

    void SoundBackgroundMusicClick()
    {
        SettingHelper.BACKGROUND_MUSIC.ENABLE = !SettingHelper.BACKGROUND_MUSIC.ENABLE;

        ShowBackgroundMusic(SettingHelper.BACKGROUND_MUSIC.ENABLE);

    }
    void SoundOnClick()
	{
		SettingHelper.SOUND.ENABLE = !SettingHelper.SOUND.ENABLE;

		ShowSound (SettingHelper.SOUND.ENABLE);

	}
	void InvitedOnClick()
	{
		SettingHelper.Instance.INVITED = !SettingHelper.Instance.INVITED;

		ShowInvited (SettingHelper.Instance.INVITED);
	}

	void ShowSound(bool _enable)
	{
		SoundOff.SetActive (!_enable);
		SoundOn.SetActive (_enable);
	}
    void ShowBackgroundMusic(bool _enable)
    {
        BackgroundOff.SetActive(!_enable);
        BackgroundOn.SetActive(_enable);
    }
    void ShowInvited(bool _invited)
	{
		InvitedOff.SetActive (_invited);
		InvitedOn.SetActive (!_invited);
	}
}
