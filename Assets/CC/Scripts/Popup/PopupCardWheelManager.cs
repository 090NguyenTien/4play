﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using SimpleJSON;
using BaseCallBack;

public class PopupCardWheelManager : MonoBehaviour {

	[SerializeField]
	GameObject panel;
	[SerializeField]
	Transform trsfLuckyWheel;
	[SerializeField]
	Button btnStart, btnBack;
	[SerializeField]
	Text txtChipUser, txtGoldUser, txtError, txtTurn, txtCostGold;
	[SerializeField]
	Image imgWheel;
	[SerializeField]
	Sprite sprLightTrue, sprLightFalse;
	[SerializeField]
	List<Image> lstImgLight;
	[SerializeField]
	ParticleSystem Particle;

	int Part = 36;
	const int Distant = 5;
	const int Loop = 10;

	float timeRemainSecond;

	int HourResetTime;
	long CostChip, CostGold;

	static Sprite sprWheel = null;
	onCallBack backClick;

	public void Init(onCallBack _backClick)
	{
		btnStart.onClick.AddListener (StartWheelOnClick);
		backClick = _backClick;

		popupFull.Init ();
	}
	int FreeTurn;
	int currentSecond;

	bool IsIdle = true;

	#region Properties

	long chip;
	private long CHIP{
		get{
			chip = MyInfo.CHIP;
			return chip;
		}
		set{
			MyInfo.CHIP = chip = value;
		}
	}
	long gold;
	private long GOLD{
		get{
			gold = MyInfo.GOLD;
			return gold;
		}
		set{
			MyInfo.GOLD = gold = value;
		}
	}
	#endregion

	enum StateError{
		NoMoney,
		ResultChip,
		ResultCard,
	}
	StateError stateError;

	void Awake()
	{
		btnBack.onClick.AddListener (BtnBackOnClick);
		isLight = true;
		for (int i = 0; i < lstImgLight.Count; i++)
			ShowLight (i, isLight);
	}
	[SerializeField]
    ParticleSystem.Particle[] particles;

	void BtnBackOnClick()
	{
		backClick ();
	}

	[SerializeField]
	PopupFullManager popupFull;

	void StartWheelOnClick()
	{
		if (!IsIdle)
			return;
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		if (FreeTurn < 1) {
			popupFull.Show ("Dùng " + CostGold + " Gold/lượt quay.", () => {
				popupFull.Hide ();
			}, () => {

				popupFull.Hide ();

				API.Instance.RequestCardWheelData ((json) => {
					Debug.Log ("Data card wheel:\n" + json);
					JSONNode node = JSONNode.Parse (json);
					FreeTurn = node ["turn"].AsInt;

					ShowTurnRemain ();

					if (FreeTurn > 0) {
						RequestStartCardWheel ("turn", RspStartCardWheel);
					} else if (GOLD < CostGold) {
						ShowError (StateError.NoMoney);
					} else {
						GOLD -= CostGold;

						ShowCashUser ();
						RequestStartCardWheel ("gold", RspStartCardWheel);
					}
				});
			});
		} else {
			API.Instance.RequestCardWheelData ((json) => {
				Debug.Log ("Data card wheel:\n" + json);
				JSONNode node = JSONNode.Parse (json);
				FreeTurn = node ["turn"].AsInt;

				ShowTurnRemain ();

				if (FreeTurn > 0) {
					RequestStartCardWheel ("turn", RspStartCardWheel);
				} else if (GOLD < CostGold) {
					ShowError (StateError.NoMoney);
				} else {
					GOLD -= CostGold;

					ShowCashUser ();
					RequestStartCardWheel ("gold", RspStartCardWheel);
				}
			});
		}


	}
	void RequestStartCardWheel(string _type, onCallBackString _callBack){
		IsIdle = false;

		API.Instance.RequestStartCardWheel (_type, RspStartCardWheel);
	}

	public void Show()
	{
		panel.SetActive (true);
		ShowCashUser ();
		IsIdle = true;
		txtError.transform.parent.gameObject.SetActive(false);

		RequestCardWheelData ();
	}
	void ShowCashUser()
	{
		txtChipUser.text = Utilities.GetStringMoneyByLong (CHIP);
		txtGoldUser.text = Utilities.GetStringMoneyByLong (GOLD);
	}
	public void Hide()
	{
		panel.SetActive (false);
	}

	public void RequestCardWheelData()
	{
		API.Instance.RequestCardWheelData (RspCardWheelData);
	}
	void RspCardWheelData(string _json)
	{
		Debug.Log (_json);
		JSONNode nodeJson = JSONNode.Parse (_json);

		JSONNode data = nodeJson ["data"];

		Part = 360 / data.Count;

//		CostChip = long.Parse (nodeJson ["chip"].Value);
		CostGold = long.Parse (nodeJson ["gold"].Value);
		txtCostGold.text = Utilities.GetStringMoneyByLong (CostGold) + "/lượt";

		FreeTurn = nodeJson ["turn"].AsInt;

		ShowTurnRemain ();

		string link = API.Instance.PrefixImage +  nodeJson ["wheel_image"].Value;

		if (sprWheel == null)
			StartCoroutine (GameHelper.Thread (link, ShowWheel));
		else
			ShowWheel (sprWheel);
	}

	void ShowTurnRemain()
	{
		txtTurn.text = FreeTurn.ToString () + " lượt quay";
	}

	void ShowWheel(Sprite _spr)
	{
		Debug.Log ("Show CardWheel");
		imgWheel.sprite = _spr;
		sprWheel = _spr;
	}

	void RspStartCardWheel (string _json)
	{
		Debug.Log (_json);

		JSONNode jsonNode = JSONNode.Parse (_json);

		FreeTurn = jsonNode ["turn"].AsInt;
		ShowTurnRemain ();

		if (jsonNode ["code"].AsInt == -1) {
			ShowError (jsonNode ["message"].Value);
		} else {
			JSONNode node = jsonNode ["rewards"];


			string type = "";
			long _value = 0;
			if (!string.IsNullOrEmpty (node ["type"])) {
				type = node ["type"].Value;
			
				_value = long.Parse (node ["value"].Value);
			}

			int index = jsonNode ["index"].AsInt - 1;


			if (jsonNode ["items"] != null) {
				JSONNode items = jsonNode ["items"];

				if (items ["chip"] != null)
					CHIP = long.Parse (items ["chip"].Value);
				if (items ["gold"] != null)
					GOLD = long.Parse (items ["gold"].Value);
				if (items ["vippoint"] != null)
					MyInfo.VIPPOINT = int.Parse (items ["vippoint"].Value);

				SFS.Instance.SendZoneRefreshData ();
			}
			StartLuckyWheel (index, () => {
				
				if (type.Equals("chip")){
					ShowError(StateError.ResultChip, _value);
				}
				else if (type.Equals("card")){
					ShowError(StateError.ResultCard, _value);
				}
					
				ShowCashUser();

				IsIdle = true;
			});
		}
	}

	public Ease easeType = Ease.InOutCubic;
	public float TimeWheel = 7;
	void StartLuckyWheel(int _index, onCallBack _callBack = null)
	{
		Vector3 result = GetRotationZ (_index);

		trsfLuckyWheel.DORotate (result, TimeWheel, RotateMode.FastBeyond360).SetEase (easeType).OnComplete (() => {
			if (_callBack != null);
			_callBack();

			if (lightThread != null)
				StopCoroutine(lightThread);
		});

		lightThread = StartCoroutine (LightThread (TimeWheel));
	}

	void ShowError(StateError _state, long _value = 0)
	{
		string _msg = "";
		switch (_state) {
		case StateError.NoMoney:
			_msg = "Số Gold không đủ. Vui lòng nạp thêm.";
			break;
		case StateError.ResultChip:
			string s1 = "Chúc mừng bạn may mắn nhận được\n";
			string s2 = " chip";
			_msg = s1 + Utilities.GetStringMoneyByLong (_value) + s2;
			break;
		case StateError.ResultCard:
			string s11 = "Chúc mừng bạn may mắn nhận được\nthẻ cào ";
			string s22 = "\nVui lòng kiểm tra hộp thư.";
			_msg = s11 + Utilities.GetStringMoneyByLong (_value) + s22;
			break;
		}
		txtError.text = _msg;
		txtError.transform.parent.gameObject.SetActive (true);

		StartCoroutine (GameHelper.Thread (3, () => {
			txtError.transform.parent.gameObject.SetActive(false);
		}));
	}
	void ShowError(string _msg)
	{
		txtError.text = _msg;
		txtError.transform.parent.gameObject.SetActive (true);

		StartCoroutine (GameHelper.Thread (3, () => {
			txtError.transform.parent.gameObject.SetActive(false);
		}));
	}

	bool isLight = true;
	public float deltaTimeWheel = .1f;
	public float distantTime = .1f;
	Coroutine lightThread;
	IEnumerator LightThread(float _time)
	{
		Debug.Log ("TIME - " + _time);
		float deltaTime = deltaTimeWheel;
		while (_time > -1) {
			for (int i = 0; i < lstImgLight.Count; i++) {
				ShowLight (i, isLight);
			}
			isLight = !isLight;

			deltaTime += _time < 3 ? deltaTime : 0;
			yield return new WaitForSeconds (deltaTime);

			_time -= deltaTime;
		}
	}
	void ShowLight(int _index, bool _isOdd)
	{
		lstImgLight [_index].sprite = _index % 2 == 0 == _isOdd ? sprLightTrue : sprLightFalse;
	}

	Vector3 GetRotationZ(int _index)
	{
		int min = Part * _index + Distant;
		int max = Part * (_index + 1) - Distant;
		Debug.Log (min + " - " + max);
		int rotation = Random.Range (min, max) - 360;

		float result = (rotation - (Loop * 360));
//		Debug.Log ("rotation: " + rotation);
//		Debug.Log (Loop * -360);
//		Debug.Log (result);
		return new Vector3 (0, 0, result);
	}
}
