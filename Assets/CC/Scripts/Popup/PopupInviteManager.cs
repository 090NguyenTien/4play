﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using System.Collections.Generic;

public class PopupInviteManager : MonoBehaviour {
		
		[SerializeField]
		GameObject panel, objItemPlayer;
		[SerializeField]
		Transform trsfItemParent;
		[SerializeField]
		Button btnClose;

		onCallBack _closeOnClick;

	List<int> lstId;

		public void Init()
		{
				btnClose.onClick.AddListener (CloseOnClick);
		}
		void CloseOnClick()
		{
			SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

				_closeOnClick ();
		}

	void ItemChildOnClick(int _id)
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		int index = lstId.IndexOf (_id);
//		Debug.Log (index);
		if (index > -1 && index < trsfItemParent.childCount) {
			Destroy (trsfItemParent.GetChild (index).gameObject);
			lstId.Remove (_id);
		}
		
				GameObject.FindGameObjectWithTag ("GameController").SendMessage ("ItemPlayerInviteOnClick", _id);
	}

		public void Show(onCallBack _callBack, onCallBack _close)
		{
		
				panel.SetActive (true);

				if (_closeOnClick == null)
						_closeOnClick = _close;
		}
	//id#name#chip#avatar
	public void ShowItems(GamePacket _packet)
	{

		string userList = _packet.GetString (ParamKey.USER_LIST);

		if (string.IsNullOrEmpty (userList))
			return;

		string[] _infos = userList.Split ('$');

		lstId = new List<int> ();
		for (int i = 0; i < _infos.Length; i++) {
			ItemPlayerInviteView item;

			if (i < trsfItemParent.childCount)
				item = trsfItemParent.GetChild (i).GetComponent<ItemPlayerInviteView> ();
			else {
				GameObject obj = Instantiate (objItemPlayer) as GameObject;
				obj.transform.SetParent (trsfItemParent);
				obj.transform.localScale = Vector3.one;

				item = obj.GetComponent<ItemPlayerInviteView> ();
			}

			string[] info = _infos [i].Split ('#');
			int id = int.Parse (info [0]);
			string name = info [1];
			long chip = long.Parse (info [2]);
			string urlAvatar = info [3];
			int vipPoint = int.Parse (info [4]);

			item.Init (i / 2 == 1, id, urlAvatar, name, chip, vipPoint, ItemChildOnClick);

			lstId.Add (id);

		}

		ClearAllitem (_infos.Length);
	}

		public void Hide()
		{
				panel.SetActive (false);
		}

		public void ClearAllitem (int _indexFrom = 0)
	{
//		Debug.Log (_indexFrom + " - ");
		for (int i = _indexFrom; i < trsfItemParent.childCount; i++) {
			GameObject.Destroy (trsfItemParent.GetChild (i).gameObject);
		}
	}
}
