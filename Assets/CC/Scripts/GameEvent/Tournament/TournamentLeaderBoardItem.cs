﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TournamentLeaderBoardItem : MonoBehaviour
{
    [SerializeField]
    private Image itemBg = null; 

    [SerializeField]
    private Text txtRankIndex = null, txtUserName = null, txtUserValue = null, txtUserGift = null;

    [SerializeField]
    private Sprite spriteStrong = null;
    public void initLeaderboardItem(int rankIndex, ISFSObject itemInfo, TournamentType tournamentType)
    {
        string userName = itemInfo.GetUtfString("username");
       
        txtRankIndex.text = rankIndex.ToString();
        txtUserName.text = userName;
        if (tournamentType == TournamentType.MoneyWin)
        {
            long userValue = itemInfo.GetLong("value");
            txtUserValue.text = Utilities.GetStringMoneyByLong(userValue);
        }
        else
        {
            long userValue = itemInfo.GetInt("value");
            txtUserValue.text = userValue.ToString();
        }

        string userGift = "";
        ISFSObject data = itemInfo.GetSFSObject("gift");

        if (data.ContainsKey("card") == true)
        {
            userGift += "Thẻ cào: " + data.GetInt("card");
        }

        if (data.ContainsKey("wheel") == true)
        {
            userGift += " Vòng quay: " + data.GetInt("wheel").ToString();
        }

        if (data.ContainsKey("chip") == true)
        {
            userGift += " Chip: " + Utilities.GetStringMoneyByLong(data.GetInt("chip"));
        }

        txtUserGift.text = userGift;
        if (rankIndex % 2 == 0)
        {
            itemBg.sprite = spriteStrong;
        }

        gameObject.SetActive(true);
    }
}