﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.UI;

public class PhomColor
{
    public static Color32 TransparentColor = new Color32(0, 0, 0, 0);
    public static Color32 EatenCardColor = new Color32(255, 255, 0, 100);
    public static Color32 DrawnCardColor = new Color32(0, 255, 0, 100);
    public static Color32 DiscardedCardColor = new Color32(0, 0, 0, 120);
    public static Color32 LastDiscardedCardColor = new Color32(0, 0, 0, 60);
    public static Color32 MeldBorderColor1 = new Color32(243, 115, 240, 255);
    public static Color32 MeldBorderColor2 = new Color32(139, 209, 103, 255);
    public static Color32 MeldBorderColor3 = new Color32(247, 123, 115, 255);
}
