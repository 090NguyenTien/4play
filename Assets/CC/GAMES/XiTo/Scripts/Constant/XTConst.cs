﻿using UnityEngine;
using System.Collections;

namespace ConstXT {
		
	public class XTConst {

		public const int MAX_USER = 5; 	//Toi da 5 nguoi choi
		public const int FIRST_DEAL_CARDS_COUNT = 2;	//Luot chia bai dau tien: 2 la
		public const int MAX_HANDCARDS = 5;	//Bai tren tay, toi da 5 la

	}


	public enum XTAction
	{
		Call,
		Raise,
		RaiseMulti2,
		RaisePart4,
		RaisePart2,
		RaiseAll,

		Fold,
		Check,
	}
}