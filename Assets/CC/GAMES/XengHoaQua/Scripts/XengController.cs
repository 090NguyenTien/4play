﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using SimpleJSON;

public class XengController : MonoBehaviour {


    int[] aListRotation = new int[24];//mang chua vi tri rotate tuong ứng của từng Xèng trên bàn
    [SerializeField]
    List<Image> lstImgOverEffect;
    [SerializeField]
    List<Text> lstTextRatio, lstTextBet;
    [SerializeField]
    Text txtMoneyWin, txtMoneyTotal, txtTaiXiu, txtDiamond;
    bool isRun, isLuckRun;
    int resultId;
    [SerializeField]
    Image barRotate;
    [SerializeField]
    PopupExchangeXeng panelExchange;
    bool isNewTurn;//Bien Xac Đinh Đã Kết Thúc 1 Vòng Chơi Và Được Phép Cược Ván Mới
    bool isNew;//bat dau cược van moi
    bool isBet;//Biến kiểm tra bạn đã chọn cược hay chưa
    bool autoRun;
    bool isFirstRun;//Đã chạy lần đầu chưa - để set vụ autoRun cho chính xác
    bool isRotateComplete;
    public static int GemToPoint;
    public static int PointToGem;
    [SerializeField]
    HoldButtonHelper holdButtonHelper;
    [SerializeField]
    GameObject panelBlack, BetHint, GoHint, TaixiuHint;
    [SerializeField]
    List<GameObject> lstGlowWin;
    [SerializeField]
    Toggle checkAuto;
    [SerializeField]
    PopupShopManager Shop;
    internal void UpdateInfoBar()
    {
        txtMoneyTotal.text = MyInfo.BAR_POINT.ToString();
        txtDiamond.text = MyInfo.GEM.ToString();
    }
    void Hint(bool black, bool bet, bool go, bool taixiu)
    {
        panelBlack.SetActive(black);
        BetHint.SetActive(bet);
        GoHint.SetActive(go);
        TaixiuHint.SetActive(taixiu);
    }
    public void Close()
    {
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.HomeSceneV2);
    }
   
    //int curTotalPoint=0;//số điểm hiện tại của user nhận từ service
    // Use this for initialization
    void Start () {
        if (!GameHelper.IS_LOAD_LOGIN)
        {
            GameHelper.ChangeScene(GameScene.LoginScene);
            return;
        }
        LoadingManager.Instance.ENABLE = false;
        Hint(true, false, false, false);
        //LoadingManager.Instance.setPlayMusic(false);
        SoundManager.PlaySound(SoundManager.BAR_BgXeng,true);
        for (int i = 0; i < 24; i++)
        {
            aListRotation[i] = 360-(i * 15);
            lstImgOverEffect[i].gameObject.SetActive(false);
            lstImgOverEffect[i].transform.GetChild(0).GetComponentInChildren<Animator>().Play("NoAction");
        }
        HideEffectWin();
        API.Instance.RequestGetBarGameData(GetDataBarComplete);
        txtMoneyWin.text = "0";
        panelExchange.Hide();
        isNewTurn = true;

        Shop.Init(UpdateInfoUser, BtnBackShopOnClick);
    }

    void BtnBackShopOnClick()
    {
        Shop.Hide();
    }
    public void ShopOnClick(bool ByGem = false)
    {        
        Shop.Show(ByGem);        
    }
    private void UpdateInfoUser()
    {
        txtDiamond.text = MyInfo.GEM.ToString();
    }

    void HideEffectWin()
    {
        for(int i=0;i< lstGlowWin.Count; i++)
        {
            lstGlowWin[i].SetActive(false);
            lstGlowWin[i].GetComponentInChildren<Animator>().Play("NoAction");            
        }
    }
    public void ShowExchangePanel()
    {
        if(isBet || isRun || !isNewTurn)
        {
            AlertController.api.showAlert("Không Thể Quy Đổi Khi Đang Chạy. Vui Lòng Đợi Kết Thúc Ván!");
            return;
        }
        panelExchange.Show();
    }
    public void BetItem(int id)
    {
        BetItemUI(id);
        holdButtonHelper.Remove();
    }
    public void HoldBetItem(int id)
    {
        holdButtonHelper.Run(BetItemUI,id);
    }
    void BetItemUI(int id)
    {
        if (isRun)
        {
            return;
        }else
        {
            if (autoRun) return;
        }
        if (!isNewTurn) return;
        SoundManager.StopSound(SoundManager.BAR_BgXeng);
        switch (id)
        {
            case 1: SoundManager.PlaySound(SoundManager.BAR_BET0); break;
            case 2: SoundManager.PlaySound(SoundManager.BAR_BET1); break;
            case 3: SoundManager.PlaySound(SoundManager.BAR_BET2); break;
            case 4: SoundManager.PlaySound(SoundManager.BAR_BET3); break;
            case 5: SoundManager.PlaySound(SoundManager.BAR_BET4); break;
            case 6: SoundManager.PlaySound(SoundManager.BAR_BET5); break;
            case 7: SoundManager.PlaySound(SoundManager.BAR_BET6); break;
            case 8: SoundManager.PlaySound(SoundManager.BAR_BET7); break;
        }
        //Debug.LogError("1111111");
        if (txtMoneyWin.text != "0") return;
        //Debug.LogError("222222222");
        if (int.Parse(txtMoneyTotal.text) == 0)
        {
            AlertController.api.showAlert("Bạn Không Đủ Điểm Để Đặt! Vui Lòng Quy Đổi Kim Cương Để Lấy Thêm Điểm!");
            return;
        }
        //Debug.LogError("3333333");
        sCurBet = "";
        if (!isBet && isNew)
        {
            isNew = false;
            clearBetUI();
        }
        HideEffectWin();
        Hint(true, true, true, false);
        int val = int.Parse(lstTextBet[id - 1].text);
        lstTextBet[id - 1].text = (val + 1).ToString();
        txtMoneyTotal.text = (int.Parse(txtMoneyTotal.text) - 1).ToString();
        isBet = true;
    }
    public void BetAllIn()
    {
        BetAllInUI();
        holdButtonHelper.Remove();
    }
    public void HoldBetAllIn()
    {
        holdButtonHelper.Run(BetAllInUI);
    }
    void BetAllInUI(int param=-1)
    {
        if (isRun)
        {
            return;
        }
        else
        {
            if (autoRun) return;
        }
        if (!isNewTurn) return;
        SoundManager.StopSound(SoundManager.BAR_BgXeng);
        SoundManager.PlaySound(SoundManager.BAR_BET7);
        if (txtMoneyWin.text != "0") return;
        if (int.Parse(txtMoneyTotal.text) < 8)
        {
            AlertController.api.showAlert("Bạn Không Đủ Điểm Để Đặt! Vui Lòng Quy Đổi Kim Cương Để Lấy Thêm Điểm!");
            return;
        }
        sCurBet = "";
        if (!isBet && isNew)
        {
            isNew = false;
            clearBetUI();
        }
        HideEffectWin();
        Hint(true, true, true, false);
        for (int i = 0; i < 8; i++)
        {
            int val = int.Parse(lstTextBet[i].text);
            lstTextBet[i].text = (val + 1).ToString();
        }
        txtMoneyTotal.text = (int.Parse(txtMoneyTotal.text) - 8).ToString();
        isBet = true;
    }
    void clearBetUI()
    {
        for (int i = 0; i < 8; i++)
        {
            lstTextBet[i].text = "0";
        }
    }
    bool isTaixiu;
    public void TaiXiu(int type)//1 la tai 2 la xiu
    {
        if (int.Parse(txtMoneyWin.text) < 1 || autoRun) return;
        if (isTaixiu || isLuckRun) return;
        isTaixiu = true;
        Debug.Log("TaiXiu====Type" + type);
        HideEffectWin();
        API.Instance.RequestBarTaiXiu(type,int.Parse(txtMoneyWin.text),TaiXiuBarComplete);
    }

    private void TaiXiuBarComplete(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" TaiXiuBarComplete " + _json);
        if (node["status"].AsInt == 1)
        {
            MyInfo.BAR_POINT = node["current_point"].AsInt;
            //animate Tai xiu
            StartCoroutine(AnimateTaiXiu(node));

        }else
        {
            checkHintByBarPoint();
        }
        
    }

    private IEnumerator AnimateTaiXiu(JSONNode node)
    {
        SoundManager.PlaySound(SoundManager.BAR_TaiXiu,true);
        for (int i = 0; i < 20; i++)
        {
            txtTaiXiu.text = UnityEngine.Random.Range(1, 12).ToString();
            yield return new WaitForSeconds(0.1f);
        }
        SoundManager.StopSound(SoundManager.BAR_TaiXiu);
        txtTaiXiu.text = node["luckypoint"];
        if (node["point"].AsInt > 0)
        {
            curWin = node["point"].AsInt;
            txtMoneyWin.text = curWin.ToString();
            SoundManager.PlaySound(SoundManager.BAR_WinSicBo);
            Hint(true, false, true, true);
        }
        else
        {
            txtMoneyWin.text = "0";
            isNewTurn = true;
            isNew = true;
            isBet = false;
            SoundManager.PlaySound(SoundManager.BAR_Nuot);
            checkHintByBarPoint();
        }
        isTaixiu = false;
    }

    int curWin;
    public void RightWinPointTxiu()
    {
        if (isRun||autoRun) return;
        if (!isBet && int.Parse(txtMoneyWin.text) < 1 && !isNewTurn)
        {
            isNewTurn = true;
            Hint(true, true, true, false);
            return;
        }
        if (int.Parse(txtMoneyWin.text) < 1) return;
        txtMoneyWin.text = (int.Parse(txtMoneyWin.text) - 1).ToString();
        txtMoneyTotal.text = (int.Parse(txtMoneyTotal.text) + 1).ToString();
    }
    public void LeftWinPointTxiu()
    {
        if (isRun|| autoRun) return;
        if (int.Parse(txtMoneyWin.text) > curWin * 2) return;
        if (int.Parse(txtMoneyWin.text) < 1) return;
        txtMoneyWin.text = (curWin*2).ToString();
        txtMoneyTotal.text = (MyInfo.BAR_POINT - curWin*2).ToString();
    }
    Hashtable[] dataIfoBar;
    const string tao = "tao";
    const string cam = "cam";
    const string le = "le";
    const string chuong = "chuong";
    const string dua = "dua";
    const string sao = "sao";
    const string s77 = "77";
    const string bar = "bar";
    void checkHintByBarPoint()
    {
        if (MyInfo.BAR_POINT > 0)
        {
            Hint(true, true, true, false);
        }
        else
        {
            Hint(true, false, false, false);
            AlertController.api.showAlert("Bạn Không Đủ Điểm Để Chơi Vui Lòng Quy Đổi Kim Cương Để Lấy Điểm Chơi Tiếp!");
            autoRun = false;
            isNew = true;
        }
    }
    private void GetDataBarComplete(string _json)
    {
        //_json = '{"status":1,"data":{"77":{"id":7,"value":40},"tao":{"id":1,"value":5},"cam":{"id":2,"value":10},"le":{"id":3,"value":15},"chuong":{"id":4,"value":20},"dua":{"id":5,"value":20},"sao":{"id":6,"value":30},"bar":{"id":7,"value":100}},"gem_to_point":{"in":1,"out":10},"point_to_gem":{"in":10,"out":1}}';
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" GetDataBarComplete " + _json);
        if (node["status"].AsInt == 1)
        {
            GemToPoint = node["gem_to_point"]["out"].AsInt;
            PointToGem = node["point_to_gem"]["in"].AsInt;
            //if(node["current_point"]!="") MyInfo.BAR_POINT = int.Parse(node["current_point"]);
            txtMoneyTotal.text = MyInfo.BAR_POINT.ToString();
            checkHintByBarPoint();
            dataIfoBar = new Hashtable[8];
            JSONNode data = node["data"];
            string[] lstConstId = { tao, cam, le,chuong,dua,sao,s77,bar};
            int index = 0;
            foreach(string constId in lstConstId)
            {
                dataIfoBar[index] = new Hashtable();
                dataIfoBar[index].Add("id", data[constId]["id"]);
                dataIfoBar[index].Add("name", constId);
                dataIfoBar[index].Add("ratio", data[constId]["value"]);
                if (constId == tao)         lstTextRatio[0].text = data[constId]["value"];
                else if(constId == cam)     lstTextRatio[1].text = data[constId]["value"];
                else if (constId == le)     lstTextRatio[2].text = data[constId]["value"];
                else if (constId == chuong) lstTextRatio[3].text = data[constId]["value"];
                else if (constId == dua)    lstTextRatio[4].text = data[constId]["value"];
                else if (constId == sao)    lstTextRatio[5].text = data[constId]["value"];
                else if (constId == s77)    lstTextRatio[6].text = data[constId]["value"];
                else if (constId == bar)    lstTextRatio[7].text = data[constId]["value"];
                index++;
            }

        }
    }

    
    // Update is called once per frame
    void Update () {
        if (isRun)
        {
            if (isRotateComplete) return;
            float barRotatePos = barRotate.transform.eulerAngles.z;
            //barRotatePos = barRotatePos;
            //Debug.LogError(barRotatePos);
            int min;
            int max;
            for (int i = 0; i < 24; i++)
            {
                max = aListRotation[i];
                Image imgOverEffect;
                imgOverEffect = lstImgOverEffect[i];
                if (i == 23)
                {
                    min = 0;
                }
                else {
                    min = aListRotation[i + 1];
                }
                Animator animator = imgOverEffect.transform.GetChild(0).GetComponentInChildren<Animator>();
                if (barRotatePos> min && barRotatePos < max)
                {
                    if (!imgOverEffect.gameObject.activeSelf)
                    {
                        imgOverEffect.gameObject.SetActive(true);
                        animator.Play("GlowWinBar");
                    }                    
                }
                else
                {
                    if (imgOverEffect.gameObject.activeSelf)
                    {
                        imgOverEffect.gameObject.SetActive(false);
                        animator.Play("NoAction");
                    }                          
                }
            }
            return;
        }
        //if (autoRun)
       // {
           // if (sCurBet == "") return;
           // Run();
       // }
	}
    private IEnumerator AutoRunGame()
    {
        //yield return new WaitForSeconds(1.8f);
        if (autoRun)
        {
            if (sCurBet == "") yield return null;
            if (int.Parse(txtMoneyWin.text) > 0)
            {
                Run();//chuyển tiền thắng vào tiền tổng
                yield return new WaitForSeconds(1.0f);
                Run();
            }
            else
            {
                Run();
            }
        }        
    }
    public float duration = 0.1f;
    IEnumerator CountTo(int target, Text txtTarget)
    {
        int start = int.Parse(txtTarget.text);
        for (float timer = 0; timer < duration; timer += Time.deltaTime)
        {
            float progress = timer / duration;
            txtTarget.text = ((int)Mathf.Lerp(start, target, progress)).ToString();
            yield return null;
        }
        txtTarget.text = target.ToString();
    }
    string sCurBet = "";
    int curBetMoney = 0;
    public void Run()
    {
        Debug.Log("RunClick====isRun:" + isRun + " ====isTaiXiu:"+isTaixiu+ " ====isNewTurn:"+ isNewTurn + " ====isLuckRun:"+ isLuckRun);
        if (isRun || isTaixiu || isLuckRun) return;
        HideEffectWin();
        if (int.Parse(txtMoneyWin.text) > 0)
        {
            //AnimHomeController.api.effectReceiveGold(txtMoneyTotal.gameObject);
            //StopCoroutine("CountTo");
            StartCoroutine(CountTo(MyInfo.BAR_POINT, txtMoneyTotal));
            txtMoneyTotal.text = MyInfo.BAR_POINT.ToString();
            txtMoneyWin.text = "0";
            isNewTurn = true;
            isNew = true;
            isBet = false;
            Hint(true, true, true, false);
            return;
        }
        if (!isNewTurn) return;
        if (sCurBet == "")
        {
            curBetMoney = 0;
            string coma = "";
            for (int i = 0; i < 8; i++)
            {
                if (int.Parse(lstTextBet[i].text) < 1) continue;
                string id = (i + 1).ToString();
                sCurBet += coma + id.ToString() + ":" + lstTextBet[i].text;
                coma = ",";
                curBetMoney += int.Parse(lstTextBet[i].text);
            }
        }else
        {
            if (curBetMoney > int.Parse(txtMoneyTotal.text))
            {
                AlertController.api.showAlert("Bạn Không Đủ Điểm Để Chơi Vui Lòng Quy Đổi Kim Cương Để Lấy Điểm Chơi Tiếp!");
                isNew = true;
                autoRun = false;
                return;
            }else
            {
                txtMoneyTotal.text = (int.Parse(txtMoneyTotal.text) - curBetMoney).ToString();
            }            
        }
       /* if (curBetMoney > int.Parse(txtMoneyTotal.text))
        {
            AlertController.api.showAlert("Bạn Không Đủ Điểm Để Chơi Vui Lòng Quy Đổi Kim Cương Để Lấy Điểm Chơi Tiếp!");
            autoRun = false;
            return;
        }*/
        if (curBetMoney == 0)
        {
            AlertController.api.showAlert("Bạn Chưa Chọn Cược");
            return;
        }
        Debug.Log("sCurBet==" + sCurBet);
        isRun = true;
        txtTaiXiu.text = "0";
        Hint(false, false, false, false);
        isFirstRun = true;
        autoRun = checkAuto.isOn;
        isRotateComplete = false;
        API.Instance.RequestRunBarGame(sCurBet, ResponseRunGameBar);       
    }
    public void StatusAuto()
    {
        if (isFirstRun)
        {
            autoRun = checkAuto.isOn;
        }
    }
    private void ResponseRunGameBar(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" ResponseRunGameBar " + _json);
        if (node["status"].AsInt == -1)
        {
            isRun = false;
            isNewTurn = true;
            isNew = true;
            isBet = false;
            AlertController.api.showAlert("Bạn Không Đủ Điểm Để Cược");
            Hint(true, true, true, false);
            return;
        }else if (node["status"].AsInt == -2)
        {
            isRun = false;
            isNewTurn = true;
            isNew = true;
            isBet = false;
            AlertController.api.showAlert("Bạn Chưa Chọn Cược");
            Hint(true, true, true, false);
            return;
        }
        SoundManager.StopSound(SoundManager.BAR_BgXeng);
        StartCoroutine(PlaySoundRun());
        
        resultId = node["index"].AsInt;// UnityEngine.Random.Range(0, 23);
        Debug.Log("resultId==" + resultId);
        MyInfo.BAR_POINT = node["current_point"].AsInt;
        //txtMoneyWin.text = node["point"];
        curWin = node["win_point"].AsInt;
        float TimeWheel = 5;
        Ease easeType = Ease.InOutExpo;
        Vector3 result = GetRotationZ(23 - resultId);
        barRotate.transform.DORotate(result, TimeWheel, RotateMode.Fast).SetEase(easeType).OnComplete(() => {            
            SoundManager.PlaySound(SoundManager.BAR_EndRun);             
            txtMoneyWin.text = node["first_point"];
            isRotateComplete = true;
            JSONArray luckyBonus = node["lucky_bonus"].AsArray;            
            ShowEffectGlowXengWin(node["id"].AsInt);//hiển thị xèng trúng
            if (luckyBonus!=null && luckyBonus.Count>0)
            {
                Debug.Log("====co lucky nha====");
                StartCoroutine(RunLuckyBonusAnimation(luckyBonus));
            }else if (resultId==6||resultId==18)//vô 2 ô cơ hội và bị nuốt
            {
                Debug.Log("====vo o lucky====");
                StartCoroutine(RunLuckyBonusAnimation(luckyBonus,true));
            }
            else
            {
                Debug.Log("====ko co lucky====");
                if (curWin == 0)
                {
                    txtMoneyWin.text = "0";
                    isNew = true;
                    isNewTurn = true;
                    isBet = false;
                    checkHintByBarPoint();
                }else
                {
                    Hint(true, false, true, true);
                }
                StartCoroutine(IsRunDelay());
            }
        });
    }
    void ShowEffectGlowXengWin(int id)
    {
        if (id < 0) return;//vô 2 ô lucky thì ko hiển thị Glow bên Bet Board
        lstGlowWin[id - 1].SetActive(true);
        lstGlowWin[id - 1].GetComponentInChildren<Animator>().Play("GlowWinBar");
    }
    private IEnumerator RunLuckyBonusAnimation(JSONArray luckyBonus, bool isNuot=false)
    {
        isLuckRun = true;
        SoundManager.PlaySound(SoundManager.BAR_WaitLuckyShoot, true);
        yield return new WaitForSeconds(2f);
        SoundManager.StopSound(SoundManager.BAR_WaitLuckyShoot);
        if (isNuot)
        {
            SoundManager.PlaySound(SoundManager.BAR_Nuot);
            Hint(true, true, true, false);
            isLuckRun = false;
            StartCoroutine(IsRunDelay());
            yield return null;
        }
        Debug.LogWarning("RunLuckyBonusAnimation");
        foreach (JSONNode itemLucky in luckyBonus)
        {
            Debug.LogWarning("itemLucky" + itemLucky["index"].AsInt);
            lstImgOverEffect[itemLucky["index"].AsInt].gameObject.SetActive(true);
            txtMoneyWin.text = (int.Parse(txtMoneyWin.text) + itemLucky["point"].AsInt).ToString();
            SoundManager.PlaySound(SoundManager.BAR_LuckyShoot);
            ShowEffectGlowXengWin(itemLucky["id"].AsInt);
            yield return new WaitForSeconds(1.8f);
        }
        txtMoneyWin.text = curWin.ToString();
        if (curWin > 0)
        {
            Hint(true, false, true, true);
        }else
        {
            Hint(true, true, true, false);
        }
        isLuckRun = false;
        StartCoroutine(IsRunDelay());
    }
    private IEnumerator IsRunDelay()
    {
        Debug.Log("IsRunDelay=======" + isRun);
        if (autoRun)
        {
            yield return new WaitForSeconds(1.8f);
        }
        Debug.Log("IsRunDelay222=======" + isRun);
        isRun = false;
        StartCoroutine(AutoRunGame());
    }
    private IEnumerator PlaySoundRun()
    {
        SoundManager.PlaySound(SoundManager.BAR_StarRun);
        yield return new WaitForSeconds(1.8f);
        //for (int i = 0; i < 5; i++)
        //{
        SoundManager.StopSound(SoundManager.BAR_StarRun);
        SoundManager.PlaySound(SoundManager.BAR_OverRun,true);
        yield return new WaitForSeconds(1.1f);
        SoundManager.StopSound(SoundManager.BAR_OverRun);              
        SoundManager.PlaySound(SoundManager.BAR_PrepairEndRun);
        yield return new WaitForSeconds(1.3f);
        SoundManager.PlaySound(SoundManager.BAR_HitResult);
        //yield return new WaitForSeconds(0.3f);
        //SoundManager.PlaySound(SoundManager.BAR_OverRun);
        //}

    }

    const int angle = 15;//1 phan chiem 45 do
    const int Loop = 20;
    Vector3 GetRotationZ(int _index)
    {
        //int min;// = angle * (23 - _id);
        //min = 0;
        //int max = angle * _index;
        int rotation = (_index * angle)+ angle/2;
        //Debug.LogError("rotation==" + rotation);
        float result = (rotation - (Loop * 360));
        //Debug.LogError("result==" + result);
        return new Vector3(0, 0, result);
    }
}
