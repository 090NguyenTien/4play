﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListHorseMyBet : MonoBehaviour {
    [SerializeField]
    List<ItemResultHorseBet> lstItem = new List<ItemResultHorseBet>();
	// Use this for initialization
	public void Init(GamePacket gp)
    {
        ISFSArray board_bet = gp.GetSFSArray("board_bet");
        SFSObject obj;
        string id;
        int ratio; 
        for (int i = 0; i < board_bet.Size(); i++)
        {
            obj = (SFSObject)board_bet.GetSFSObject(i);
            id = obj.GetUtfString("slot");//1-2,1-3...5-6
            ratio = obj.GetInt("ratio");
            lstItem[i].Init(id,ratio);
        }
    }
    public void ClearData()
    {
        for (int i = 0; i < lstItem.Count; i++)
        {            
            lstItem[i].ClearData();
        }
    }
    public ItemResultHorseBet getHorseBetBySlot(string slot)
    {
        foreach(ItemResultHorseBet item in lstItem)
        {
            if (item.Slot == slot) return item;
        }
        return null;
    }
    public void setHorseBetBySlot(string slot,long chipBet,long chipWin)
    {
        foreach (ItemResultHorseBet item in lstItem)
        {
            if (item.Slot == slot)
            {
                item.setDataBySlot(chipBet, chipWin);
            }
        }
    }
    public void ShowHistoryBet(string slot, long chipBet, long chipWin, int index, int ratio)
    {       
        lstItem[index].ShowHistoryBet(slot, chipBet, chipWin, ratio);
    }
    
}
