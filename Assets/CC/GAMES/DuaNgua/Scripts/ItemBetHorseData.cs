﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemBetHorseData : MonoBehaviour {

    public Text TxtBet, TxtRatio, TxtBetTotal;
    public Button BtnBet;
    public string id = "";
    long sumChipBet;    
    int ratio;
    bool stopRunFake = false;
    long sumFakeChip=0;

    public bool StopRunFake
    {
        get
        {
            return stopRunFake;
        }

        set
        {
            stopRunFake = value;
        }
    }

    void Start()
    {
        gameObject.GetComponent<Animator>().Play("NoAction");
        BtnBet.onClick.AddListener(OnBetHorse);
    }
    public IEnumerator RunFakeBetAnimation()
    {
        if (StopRunFake)
        {
            yield return null;
        }
        int rand = UnityEngine.Random.Range(0, 3);
        if (rand == 1)
        {
            gameObject.GetComponent<Animator>().Play("BetHorse", 0, 0.25f);
            UpdateBetTotalFake(UnityEngine.Random.Range(1, 5) * 1000);
        }
        float randtime = UnityEngine.Random.Range(0.3f, 1f);
        yield return new WaitForSeconds(randtime);
        StartCoroutine(RunFakeBetAnimation());
    }    
    private void OnBetHorse()
    { 
        GamePacket gp = new GamePacket(CommandKey.BET);
        gp.Put("slot", id);
        gp.Put(ParamKey.BET_MONEY, DuanguaController.ChoosingBetMoney);
        SFS.Instance.SendRoomRequest(gp);               
    }
    public void SetSumBetSlot(long chipBet)
    {
        sumChipBet = chipBet;
        TxtBet.text = Utilities.GetStringMoneyByLongBigSmall(sumChipBet);
    }
    public void UpdateBetSlot(long addPrice)
    {
        sumChipBet += addPrice;
        sumFakeChip += addPrice;
        TxtBet.text = Utilities.GetStringMoneyByLongBigSmall(sumChipBet);
    }
    public void UpdateBetTotalFake(long chipFake)
    {
        sumFakeChip += chipFake;        
        TxtBetTotal.text = Utilities.GetStringMoneyByLongBigSmall(sumFakeChip);
    }
    public void SetRatioBetSlot(int ratio)
    {
        this.ratio = ratio;
        TxtBet.text = "0";
        TxtBetTotal.text = "0";
        TxtRatio.text = "x" + ratio.ToString();
        //restet sumChipBet
        sumChipBet = 0;
        sumFakeChip = 0;
    }
}
