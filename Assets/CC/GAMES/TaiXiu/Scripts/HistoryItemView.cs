﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HistoryItemView : MonoBehaviour {

    [SerializeField]
    Text TypeTxt, BetTxt, GetTxt;

    private HistoryItem item;

    public void Init(HistoryItem item)
    {
        this.item = item;
    }

    public void Show()
    {
        TypeTxt.text = item.Type;
        BetTxt.text = item.Bet;
        GetTxt.text = item.Get;
    }
}
