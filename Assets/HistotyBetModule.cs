﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistotyBetModule : MonoBehaviour {

    public Text TypeTxt, BetNumBerTxt, MoneyTxt;

    private HistoryBetItemHome item;

    public void Init(HistoryBetItemHome item)
    {
        this.item = item;
    }



    public void Show(string type, string number, string money)
    {
        TypeTxt.text = type;
        BetNumBerTxt.text = number;
        MoneyTxt.text = Utilities.GetStringMoneyByLong(long.Parse(money)); ;
    }
}
